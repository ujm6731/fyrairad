Om du vill spela mot datorn ska du skriva
```
python3 fyrairad.py cpu
```


# Fyra i rad #

Du kan använda dig av klassen som finns i matrix.py. En klass är ett object med givna metoder och atributer förknippade. Den klassen som finns i matrix.py heter Matrix. Matrix är en spelplan som du kan använda dig av i ditt fyra i rad spel. För att skapa en ny spelplan skriver du:

```
#!python
Matrix(antal rader, antal kolumner)

```
Om du sedan väljer att använda print på ett Matrix-objekt så kommer det automatiskt att skapa ett rutnät på följande sätt och siffror ovanför som i exemplet nedan:
```
#!python
matris = Matrix(10, 10)
print(matris)
 1 2 3 4 5 6 7 8 9 10
_____________________
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
| | | | | | | | | | |
---------------------
```
Du kan även få ut ett index på en given ruta igenom att skriva:
```
#!python
m = Matrix(6,7)
m[4][2]
' '
```
Den beter sig alltså nästan som en lista i en lista.
Notera att det är den nedre vänstra rutan som är 0, 0!

Matrix har även två metoder för att ta reda på hur många rader respektive kolumner en matris har. Dessa metoder heter getNumRows och getNumColumns. För att använda dessa skriver du:
```
#!python
matris = Matrix(6,7)

matris.getNumRows()
6
matris.getNumColumns()
7
```