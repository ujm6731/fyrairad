# Ett fyra-i-rad spel
#
# Gjort av Erik Stein  :D

from matrix import *
from player import *
from game import *
import logik
import ai
from sys import exit, argv
import shelve


if len(argv) > 1:
    if argv[1].lower() == "cpu":
        cpu = True
    else:
        cpu = False
else:
    cpu = False

version = "beta 3.3"

rows = 6
columns = 7

matris = Matrix(rows, columns)

def get_player_move(matris, spelare):
    valid_move = False
    while not valid_move:
        row = input("Vilken kolumn vill du lägga i?\n> ")
        if row.isnumeric():
            row = int(row)-1
        else:
            print("Det måste vara ETT TAL!")
            continue
        if logik.move_is_valid(matris, row, spelare):
            valid_move = True
    return row

def make_move(matris, spelare, names_list, game, move_nr):
    if names_list[spelare-1].lower().endswith("s"):
        print("%s tur." % names_list[spelare-1])
    else:
        print("%ss tur." % names_list[spelare-1])
    valid_move = False

    if spelare == 2:
        if cpu:
            row = game.cpu_player.cpu_move(matris, move_nr)
        else:
            row = get_player_move(matris, spelare)
    else:
        row = get_player_move(matris, spelare)

    for i in range(rows):
        if matris[i][row] == " ":
            if spelare == 1:
                matris[i][row] = game.player1.color + game.player1.token + "\033[1;m"
            elif spelare == 2:
                matris[i][row] = colors[game.player2.color] + self.player2.token + "\033[1;m"
            break

unavailiable_names = []

def available_name(name):
    return not name in unavailiable_names

def add_player_name(name, unavailiable_names):
    unavailiable_names.append(name)
    return unavailiable_names

def get_name(id):
    # Returns a valid player name
    name = ""
    while not name:
        prompt = "Vad heter spelare %s? \n> " % id
        player_name = input(prompt).strip()
    
        if player_name.isalpha() and available_name(player_name.lower()):
            name = player_name
            add_player_name(name.lower(), unavailiable_names)
        else:
            print("Ange ett annat namn, använd endast bokstäver!")

    return name

def get_std_token():
    token = ""
    while not token:
        prompt = "Ande ditt standardtecken.\n> "
        try:
            player_token = input(prompt).strip()[0]
        except IndexError:
            continue
        token = player_token

    return token

def get_players():
    players = shelve.open("players")
    p1name = get_name(1)
    if p1name in players.keys():
        p1 = players[p1name]
    else:
        p1std_token = get_std_token()
        p1 = Player(1, p1name, p1std_token)

    if cpu:
        p2 = ai.Ai(p1)
    else:
        p2name = get_name(2)
        if p2name in players.keys():
            p2 = players[p2name]
        else:
            p2std_token = get_std_token()
            p2 = Player(2, p2name, p2std_token)

    players.close()
    return p1, p2

def get_game():

    p1, p2 = get_players()
    g_file = shelve.open("games")
    if not cpu:
        game_name = p1.name + " " + p2.name
    else:
        game_name = p1.name + " " + p2.player.name
    if game_name in g_file.keys():
        g = g_file[game_name]
        return g
    if not cpu:
        game_name = p2.name + " " + p1.name
    else:
        game_name = p2.player.name + " " + p1.name
    if game_name in g_file.keys():
        g = g_file[game_name]
        return g
    g = Game(p1, p2, cpu)
    print(g)

    return g

def game(matris, game):
    names_list = [game.player1.name, game.player2.name]
    for i in range(round((rows * columns) / 2)):
    #for i in range(3):
        print(matris)
        make_move(matris, 1, names_list, game, i+1)
        if logik.has_won(matris, game.player1.token, game.player1.color):
            winner = game.player1.name
            return winner
        print(matris)
        make_move(matris, 2, names_list, game, i+1)
        if logik.has_won(matris, game.player2.token, game.player2.color):
            winner = game.player2.name
            return winner
    else:
        print(matris)
        print("Spelplanen är full! Det blev oavgjort!")

def update_stats(game, winner):
    if winner == game.player1.name:
        game.player1.games += 1
        game.player2.games += 1
        game.player1.wins += 1
        game.player2.losses += 1
        for i in game.player1.game_list:
            if i[0] == game.player2.name:
                i[1] += 1
        else:
            game.player1.game_list.append((game.player2.name, 1, 0, 0))

        for i in game.player2.game_list:
            if i[0] == game.player1.name:
                i[2] += 1
        else:
            game.player2.game_list.append((game.player1.name, 0, 1, 0))
    elif winner == game.player2.name:
        game.player2.games += 1
        game.player1.games += 1
        game.player2.wins += 1
        game.player1.losses += 1
        for i in game.player2.game_list:
            if i[0] == game.player1.name:
                i[1] += 1
        else:
            game.player2.game_list.append((game.player1.name, 1, 0, 0))

        for i in game.player1.game_list:
            if i[0] == game.player2.name:
                i[2] += 1
        else:
            game.player1.game_list.append((game.player2.name, 0, 1, 0))
    elif winner == None:
        game.player1.games += 1
        game.player2.games += 1
        game.player1.ties += 1
        game.player2.ties += 1
        for i in game.player1.game_list:
            if i[0] == game.player2.name:
                i[2] += 1
        else:
            game.player2.game_list.append((game.player1.name, 0, 0, 1))

        for i in game.player1.game_list:
            if i[0] == game.player2.name:
                i[2] += 1
        else:
            game.player1.game_list.append((game.player2.name, 0, 1, 0))

def end(game, winner):
    if winner != None:
        print(matris)
        if cpu:
            if winner == "CPU":
                print("\033[1;31mDatorn vann! Bättre lycka nästa gång!\033[1;m")
            else:
                print("\033[1;32mDu vann! Grattis!\033[1;m")
        else:
            print("\033[1;32m%s vann! Grattis!\033[1;m" % winner)

        update_stats(game, winner)

def quit(game):
    print("Sparar spel...")
    players_file = shelve.open("players")
    games_file = shelve.open("games")

    players_file[game.player1.name] = game.player1
    players_file[game.player2.name] = game.player2
    games_file[game.player1.name + " " + game.player2.name] = game

    players_file.close()
    games_file.close()
    exit()

def main():
    #pass

    g = get_game()
    g.start_game()

    winner = game(matris, g)
    end(g, winner)
    quit(g)


    old = """
    # Start
    global player1token, player2token
    print("FyraIRad %s" % version)
    name1, player1token, player1color = player_info(1)
    if cpu == True:
        name2 = "CPU"
        player2token = "O"
        player2color = "blå"

    else:
        name2, player2token, player2color = player_info(2)
    name1, player1token = logik.name_and_token_check(player1token, name1, player2token, name2)
    print("%s = %s och %s = %s" % (name1, player1token, name2, player2token))
    print("Matchen mellan %s och %s börjar nu!" % (name1, name2))
    
    # Game
    while True:
        try:
            winner = game(matris, name1, name2, player1token, player2token, player1color, player2color)
            break
        except KeyboardInterrupt:
            continue_game = input("Vill du fortsätta spela? ")
            if continue_game in ["j", "ja", "y", "ye", "yes"]:
                continue
            else:
                raise KeyboardInterrupt
    
    # End
    end(winner)
    """

if __name__ == '__main__':
    main()