class Player(object):
    """
    A Player object contains all information pertaining to a given player.
    Instances of class players are created from and bound to a Game object.
    """
    def __init__(self, id, name, std_token):
        self.id = id
        self.name = name
        self.games = 0
        self.wins = 0
        self.ties = 0
        self.losses = 0
        self.game_list = [] # (opponent, wins, losses, draws)
        self.standard_token = std_token

    def __str__(self):
        def format_result(wins, losses):
            return "(%s - %s)" % (wins, losses)
        print_str = ""
        print_str += "\n%s (%s)\n" % (self.name, self.standard_token)
        print_str += "-"*20 + "\n"
        print_str += "Spel        | %s\n" % self.games
        print_str += "Vinster     | %s (%s)\n" % (self.wins, self.rakna_ut_andel(self.wins))
        print_str += "Oavgjort    | %s (%s)\n" % (self.ties, self.rakna_ut_andel(self.ties))
        print_str += "Förluster   | %s (%s)\n" % (self.losses, self.rakna_ut_andel(self.losses))
        if len(self.game_list) == 0:
            print_str += "Motståndare | Har aldrig spelat\n"
        else:
            for i in range(len(self.game_list)):
                if i == 0:
                    print_str += "Motståndare | %s %s\n" % (self.game_list[i][0], format_result(self.game_list[i][1], self.game_list[i][2]))
                else:
                    print_str += "            | %s %s\n" % (self.game_list[i][0], format_result(self.game_list[i][1], self.game_list[i][2]))
        return print_str

    def rakna_ut_andel(self, n):
        if n == 0:
            return "0%"
        return str(round(100/(self.games/n), 2)) + "%"