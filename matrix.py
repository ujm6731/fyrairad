class Matrix(object):
    """
    Matrix(num_rows, num_columns) -> Matrix
    
    Create a new playingfield with a given number of rows and column
    Index 0, 0 is in the bottom left corner
    
    >>> m = Matrix(3,3)
    >>> print(m)
     1 2 3
    _______
    | | | |
    | | | |
    | | | |
    -------
    >>> m[0,0] = '#'
    >>> print(m)
     1 2 3
    _______
    | | | |
    | | | |
    |#| | |
    -------

    """
    
    def __init__(self, rows, columns):
        self.rows = rows
        self.columns = columns
        self.grid = [[' ' for column in range(columns)] for row in range(rows)]
        
    def __getitem__(self, item):
        """
        self.__getitem__((x)) <==> self[x]
        """
        return self.grid[-1 + item * -1]
            
        
    def __str__(self):
        """
        >>> print(Matrix(3,3))
         1 2 3
        _______
        | | | |
        | | | |
        | | | |
        -------
        """
        print_str = ''
        if self.columns < 11:
            numbers = [str(number) for number in range(1, (self.columns +1))]
            print_str += ' ' + ' '.join(numbers) + '\n'
            
        print_str += '_' * (self.columns * 2 + 1) + '\n'
        for row in self.grid:
            print_str += ''.join(['|' + cell for cell in row]) + '|' + '\n'
        print_str += '-' * (self.columns * 2 + 1)
        return print_str
        
    def __repr__(self):
        """
        x.__repr__() <==> self.grid
        """
        return str(self.grid)
        
    def __iter__(self):
        """
        Iterate over every row in self.grid
        """
        for row in self.grid:
            yield row
            
    def __eq__(self, other):
        """
        x.__eq__(y) <==> x.grid == y.grid
        """
        if type(self) == type(other):
            if self.grid == other.grid:
                return True
        return False
        
    def getNumRows(self):
        return self.rows
        
    def getNumColumns(self):
        return self.columns