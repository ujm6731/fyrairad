import logik
import fyrairad
from player import *
from matrix import Matrix
from random import randint, choice

class Ai(object):

    def __init__(self, player1):

        self.player = Player(2, "CPU", "0")
        self.player1 = player1

        self.rows = fyrairad.rows
        self.columns = fyrairad.columns
        self.not_columns = [set()]
        self.all_columns = set([x for x in range(self.columns)])
        self.column_list = []

    def __str__(self):
        print(self.player)

    def copy_matris(self, matris):
        matris2 = Matrix(rows, columns)
        for i in range(rows):
            for x in range(columns):
                matris2[i][x] = matris[i][x]
        return matris2

    def possible_win(self, matris, playertoken, color):
        for i in range(columns):
            matris2 = copy_matris(matris)
            for x in range(rows):
                if matris[x][i] == " ":
                    matris2[x][i] = fyrairad.colors[color] + playertoken + "\033[1;m"
                    if logik.has_won(matris2, playertoken, color):
                        return (True, i)
                    break
        return (False,)

    def possible_win_in_two(self, matris, nr1token, nr2token, nr1color, nr2color):
        posibl_columns = [(False,)]
        for i in range(columns):
            matris2 = copy_matris(matris)
            for x in range(rows):
                if matris[x][i] == " ":
                    matris2[x][i] = fyrairad.colors[nr1color] + nr1token + "\033[1;m"
                    second_move = possible_win(matris2, nr2token, nr2color)
                    if not second_move[0]:
                        break
                    else:
                        if type(posibl_columns[0]) == tuple:
                            del posibl_columns[0]
                        posibl_columns.append(i)
        if posibl_columns[0] != (False,):
            return tuple(posibl_columns)
        else:
            return posibl_columns[0]

    def cpu_move(self, matris, move_nr):
        global not_columns, column_list
        valid_move = False
        counter = 0
        while not valid_move:
            priority = 9

            column = possible_win(matris, self.player.token, self.player.color) # If cpu can win
            #print("1: " + str(column))
            if column[0] == False:
                column = possible_win(matris, self.player1.token, self.player1.color) # If player can win
                #print("2: " + str(column))
            else:
                priority = 1
            if column[0] == False:
                not_columns = possible_win_in_two(matris, self.player.token, self.palyer1.token, self.player.color, player1color) # If cpu lies there, player will win
                column_list = possible_win_in_two(matris, self.player.token, self.player.token, self.player.color, self.player.color) # If cpu can win in 2
            else:
                priority = 2

            if type(not_columns) != list:
                not_columns = list(not_columns)

            print("nc: " + str(not_columns))

            for i in range(len(not_columns)):
                if not_columns[i] == False and str(not_columns[i]) != "0":
                    del not_columns[i]

            if type(not_columns) != set:
                not_columns = set(not_columns)

            print("nc2: " + str(not_columns))

            possible_columns = all_columns - not_columns

            print("possible: " + str(possible_columns))
            print("col: " + str(column))
            print("col_list 0: " + str(column_list[0]))

            if column[0] == False:
                if str(column_list[0]).isnumeric():
                    column = column_list[randint(0, len(column_list)-1)]
                    priority = 3
                elif column_list[0] == True:
                    column = column_list[randint(1, len(column_list))]
                    priority = 3

            print("type col: " + str(type(column)))

            if type(column) == tuple:
                if column[0] == False:
                    column = randint(0, 6) # Random move, mostly used at the beginning
                    priority = 4

            try:
                print("priority: " + str(priority))
            except:
                print("priority error")

            if column not in possible_columns and priority > 2:
                if counter < 20:
                    counter += 1
                    continue
                else:
                    print("infinite loop") # Cpu wants to stop player but if he does player will win

            if move_nr == 1:
                if matris[0][3] == " ":
                    column = 3
                else:
                    column = choice([2, 4])

            try:
                print("not_column: " + str(not_columns))
                print("column: " + str(column))
            except:
                print("printing errors")

            if logik.move_is_valid(matris, column, 2):
                valid_move = True
                for i in range(rows):
                    if matris[i][column] == " ":
                        matris[i][column] = fyrairad.colors[self.player.color] + self.player.token + "\033[1;m"
                        break