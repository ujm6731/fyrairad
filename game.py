class Game(object):

    """
    A game object contains all information pertaining to a series of games between two players.
    """

    def __init__(self, player1, player2, cpu):

        self.colors = {"grå":[True, "\033[1;30m"],
            "röd":[True, "\033[1;31m"],
            "grön":[True, "\033[1;32m"],
            "gul":[True, "\033[1;33m"],
            "blå":[True, "\033[1;34m"],
            "magenta":[True, "\033[1;35m"],
            "cyan":[True, "\033[1;36m"],
            "vit":[True, "\033[1;37m"]
            }

        self.unavailiable_tokens = []

        self.player1 = player1
        if cpu:
            self.player2 = player2.player
            self.cpu_player = player2
        else:
            sefl.player2 = player2

        self.games = 0
        self.player1wins = 0
        self.player2wins = 0
        self.ties = 0
        self.cpu = cpu

    def __str__(self):
        print_str = ""
        print_str += "\n%s vs %s\n" % (self.player1.name, self.player2.name)
        print_str += "Matcher: %s\n" % self.games
        print_str += "Vinster: %s: %s\n" % (self.player1.name, self.player1wins)
        print_str += "         %s: %s\n" % (self.player2.name, self.player2wins)
        print_str += "Oavgjort: %s\n" % self.ties
        return print_str

    def start_game(self):
        if self.cpu:
            self.player2.token = "O"
            self.add_player_token(self.player2.token)
            self.player2.color = "blå"
            self.colors[self.player2.color][0] = False

        self.player1.token = self.get_token()
        self.player1.color = self.pick_color()
        if not self.cpu:
            self.player2.token = self.get_token()
            self.player2.color = self.pick_color()

    def available_token(self, token):
        return not token in self.unavailiable_tokens

    def add_player_token(self, token):
        self.unavailiable_tokens.append(token)

    def get_token(self):
        # returns a valid token, default provided if none selected
        token = ""
        while not token:
            prompt = "Ange tecknet under vilket du skall segra! Lämna tomt för standardtecknet.\n> "
            try:
                player_token = input(prompt).strip()[0]
            except IndexError:
                player_token = self.standard_token
            if self.available_token(player_token):
                token = player_token
                self.add_player_token(token)
            else:
                print("Du kan inte ha samma tecken som din motståndare!")

        return token

    def pick_color(self):
        # returns a valid color
        choice = ""
        def set_color(x):
            return self.colors[x][1] + x + "\033[1;m"
        valid_colors = [x for x in self.colors.keys() if self.colors[x][0]]
        valid_colors_colored = [set_color(x) for x in self.colors.keys() if self.colors[x][0]]
        while not choice:
            print(str(valid_colors_colored))
            print("Vilken färg vill du ha? Välj mellan " + ", ".join(valid_colors_colored) + "\033[1;m.")
            player_choice = input("> ")
            if player_choice in valid_colors:
                choice = player_choice
                self.colors[choice][0] = False
            else:
                print("Du kan inte ha samma färg som spelare 1!")

        return choice