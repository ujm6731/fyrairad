import fyrairad

rows = fyrairad.rows
columns = fyrairad.columns

colors = {"grå":[True, "\033[1;30m"],
            "röd":[True, "\033[1;31m"],
            "grön":[True, "\033[1;32m"],
            "gul":[True, "\033[1;33m"],
            "blå":[True, "\033[1;34m"],
            "magenta":[True, "\033[1;35m"],
            "cyan":[True, "\033[1;36m"],
            "vit":[True, "\033[1;37m"],
            "":[True, ""]
            }

def move_is_valid(matris, row, spelare):
    if not row in range(columns):
        if not fyrairad.cpu:
            print("Det ligger utanför spelplanen.")
        elif fyrairad.cpu and spelare == 1:
            print("Det ligger utanför spelplanen.")
        return False
    else:
        if matris[int(rows)-1][row] != " ":
            if not fyrairad.cpu:
                print("Raden är full!")
            elif fyrairad.cpu and spelare == 1:
                print("Raden är full!")
            return False
        else:
            return True

def check_rows(matris):
    for i in range(rows): # 6 (0-5)
        for x in range(columns-3): # 4 (0-3)
            if not " " in [matris[i][x+y] for y in range(4)]: #[matris[i][x], matris[i][x+1], matris[i][x+2], matris[i][x+3]]:
                if matris[i][x] == matris[i][x+1] and matris[i][x+1] == matris[i][x+2] and matris[i][x+2] == matris[i][x+3]:
                    return True

def check_columns(matris):
    for i in range(columns): # 7 (0-6)
        for x in range(rows-3): # 3 (0-2)
            if not " " in [matris[x+y][i] for y in range(4)]: #[matris[x][i], matris[x+1][i], matris[x+2][i], matris[x+3][i]]:
                if matris[x][i] == matris[x+1][i] and matris[x+1][i] == matris[x+2][i] and matris[x+2][i] == matris[x+3][i]:
                    return True

def diagonal(matris, start_column, start_row, direction=""):
    token_str = ""
    x = start_row
    y = start_column
    if start_column in range(3):
        for i in range(rows):
            try:
                token_str += matris[x+i][y+i]
            except IndexError:
                break
    elif start_column in range(columns-3, columns):
        for i in range(rows):
            try:
                token_str += matris[x+i][y-i]
            except IndexError:
                break
    else:
        if direction == "right":
            for i in range(rows):
                try:
                    token_str += matris[x+i][y+i]
                except IndexError:
                    break
        elif direction == "left":
            for i in range(rows):
                try:
                    token_str += matris[x+i][y-i]
                except IndexError:
                    break
    
    
    return token_str

def has_won(matris, token, color):
    if check_rows(matris):
        return True
    if check_columns(matris):
        return True
    for i in range(columns):
        if i == 0 or i == columns-1:
            for x in range(rows-3):
                #if (colors[color][1] + token + "\033[1;m")*4 in diagonal(matris, i, x):
                if token*4 in diagonal(matris, i, x):
                    return True
        elif i in range(1, 3) or i in range(columns-3, columns-2):
            #if (colors[color][1] + token + "\033[1;m")*4 in diagonal(matris, i, 0):
            if token*4 in diagonal(matris, i, 0):
                return True
        else:
            #if (colors[color][1] + token + "\033[1;m")*4 in diagonal(matris, i, 0, "right"):
            if token*4 in diagonal(matris, i, 0, "right"):
                return True
            #if (colors[color][1] + token + "\033[1;m")*4 in diagonal(matris, i, 0, "left"):
            if token*4 in diagonal(matris, i, 0, "left"):
                return True
    return False